<?php
/**
 * Utilisations de pipelines par formidable_imagecrop
 *
 * @plugin     formidable_imagecrop
 * @copyright  2022
 * @author     Lebardix
 * @licence    GNU/GPL
 * @package    SPIP\formidable_imagecrop\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Ajouter la CSS dans le head du site public
 * @param string $flux
 * @param bool $prive
 * @return string
 */
function formidable_imagecrop_insert_head_css($flux) {
	$flux.='<link rel="stylesheet" type="text/css" href="'.find_in_path('css/formidable_imagecrop.css').'" media="screen" />'."\n";
	return $flux;
}

function formidable_imagecrop_insert_head($flux) {
	$flux.='<script src="'.find_in_path('javascript/jquery.cropit.js').'" type="text/javascript"></script>'."\n";
	return $flux;
}

?>