# formidable_imagecrop

Ce plugin intègre a spip la librairie jquery Cropit, permettant de modifier les images incluses dans les réponses de formulaires Formidable …

La configuration est adaptée au traitement de photo "type photo d'identité", une version ultérieure permettra de définir un ou plusieurs formats de photos

Seules les photos jpeg ou png sont prises en charge

Ce plugin est destiné aux administrateurs (ou administrateurs restreints) qui gérent les données de formulaires formidable
afin de pouvoir, sans autre outil, recadrer/redimensionner les photos déposées depuis un formulaire.

En effet lors de demande depuis un formulaire de joindre des photos (trombinoscope par exemple), 
les visiteurs, malgré des spécifications que l'on peut avoir exposées, téléchargent depuis moultes terminal/applications des
photos qui ne respectent pas les consignes ( moins de 1% de documents corrects -cadrage/format/sujet/...- ) 
et mettent en cause les traitements ultérieurs qui vous appliquer sur ces éléments (par exemple création de carte adhérent)

Il est plus simple d'intervenir au niveau des photos du formulaire

Le plugin met donc directement à jour les éléments images du formulaire.  

## SOURCES :

http://scottcheng.github.io/cropit/

## Utilisation

Il suffit de cliquer sur l'image d'une colonne d'un tableau formidable pour ouvrir une page où l'on peut
retourner, agrandir la partie de l'image qui nous interresse.
Ensuite le bouton Submit ( les boutons Tourner aussi) pour enregistrer la nouvelle image.
Bien entendu on peut procéder à la modification de l'image sur toute page spip qui expose l'image avec #VOIR_REPONSE{}

Au retour sur la page d'origine (exemple d'un tableau Formidable), il faudra recharger la page pour voir les modifications apportées

## Contraintes
Les informations du formulaire d'origine ne sont pas modifiées, cad nom du fichier, type et dimensions ne sont pas mises à jour...

## ATTENTION
parfois les images postées par un internaute ne correspondent pas au type mime désigné par l'extension du nom...
ou bien celles-ci sont trop petites par rapport aux dimensions  du recadrage,
donc elles n'apparaissent pas dans la fenêtre de cadrage.

On peut opérer aussi en deux ou plusieurs étapes : si le zomm n'est pas suffisant, on édite une première fois la photo 
avec un cadrage permettant une sélection future, on enregistre, on réouvre la photo depuis le formulaire...

## TODO :
- proposer un schéma de configuration spécifiant les dimensions de l'image souhaitée.
- ne pas enregistrer l'image avec les boutons "Tourner" mais seulement "Submit" 
- probleme identifié avec l'insertion du css ...
- ajout de message  d'erreur pour photos inadaptées
- rafraichir automatiquement la page d'origine

## Merci de vos avis et conseils sur le plugins
dev débutant, je vous remercie de tous conseils d'amélioration
